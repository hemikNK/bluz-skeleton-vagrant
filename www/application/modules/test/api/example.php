<?php
/**
 * Example of API method
 *
 * @author   Anton Shevchuk
 * @created  15.10.12 15:22
 */
namespace Application;

use Application\Rpc\Rpc;
use Bluz\View\View;
use Junior\Client;
use Junior\Clientside\Request;
use Junior\Server;

return
/**
 * @param int $num
 * @return \closure
 */
function ($num) {
    return $num*$num;
};
