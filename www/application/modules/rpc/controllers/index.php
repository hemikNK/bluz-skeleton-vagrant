<?php
namespace Application;

use Application\Rpc\Rpc;
use Bluz\View\View;
use Junior\Client;
use Junior\Clientside\Request;
use Junior\Server;

return
    /**
     * @accept JSON
     * @accept HTML
     *
     * @var Bootstrap $this
     * @return void
     */
    function ($data) use ($view) {
        /**
         * @var Bootstrap $this
         * @var View $view
         */
        if ($data) {
        

            // create a new instance of Junior\Server with an instance of your class
            $server = new Server(new Rpc());
            // call process()
            $server->process();


            // create a new instance of JuniorClient with the URI of te
            $client = new Client("http://skeleton.lo/rpc/data");
            // you can use the magic method shortcut to make requests...
            //$response = $client->foo(); // --> "bar"
            // $response = $client->sum(1, 2, 3); // --> 6
            $view->text = 'qqqqqq';


            /*  // ...and it supports positional arguments

              // for named parameters you need to make a request object and send it with the client
              $request = new Request('makeFullName', array('last_name' => 'Fry', 'first_name' => 'Philip J.'));
              $response = $client->sendRequest($request); // --> "Philip J. Fry"
              // notifications should be specified when you create a request object
              $request = new Request('notify', 10, true);
              $response = $client->sendNotify($request); // --> true (on success)
              // batches are sent as an array of requests, and are processed and returned in order (with no notifications)
              $requests = [];
              $requests[] = new Request('makeFullName', array('last_name' => 'Fry', 'first_name' => 'Philip'));
              $requests[] = new Request('notify', 10, true);
              $requests[] = new Request('isEven', 11);
              $response = $client->sendBatch($requests);
              print_r($response->results); // array( "Philip J. Fry", false)*/
        }
    };
